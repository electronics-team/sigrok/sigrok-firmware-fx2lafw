From: Steve Markgraf <redacted>
Date: Sat, 13 Jan 2024 22:21:01 +0000 (+0100)
Subject: Add Hantek PSO2020 firmware support
Origin: upstream, https://sigrok.org/gitweb/?p=sigrok-firmware-fx2lafw.git;a=commitdiff_plain;h=0f2d3242ffb5582e5b9a018ed9ae9812d517a56e

Add Hantek PSO2020 firmware support
---

diff --git a/Makefile.am b/Makefile.am
index 30c9c9bb..9cc9d23e 100644
--- a/Makefile.am
+++ b/Makefile.am
@@ -46,6 +46,7 @@ dirstamps = \
 	hw/cypress-fx2/$(dirstamp) \
 	hw/hantek-6022be/$(dirstamp) \
 	hw/hantek-6022bl/$(dirstamp) \
+	hw/hantek-pso2020/$(dirstamp) \
 	hw/instrustar-isds205b/$(dirstamp) \
 	hw/sainsmart-dds120/$(dirstamp) \
 	hw/saleae-logic/$(dirstamp) \
@@ -63,6 +64,7 @@ firmware_binaries = \
 	hw/cypress-fx2/fx2lafw-cypress-fx2.fw \
 	hw/hantek-6022be/fx2lafw-hantek-6022be.fw \
 	hw/hantek-6022bl/fx2lafw-hantek-6022bl.fw \
+	hw/hantek-pso2020/fx2lafw-hantek-pso2020.fw \
 	hw/instrustar-isds205b/fx2lafw-instrustar-isds205b.fw \
 	hw/sainsmart-dds120/fx2lafw-sainsmart-dds120.fw \
 	hw/saleae-logic/fx2lafw-saleae-logic.fw \
@@ -98,6 +100,10 @@ hantek_6022bl_headers = $(scope_headers)
 hantek_6022bl_sources = hw/hantek-6022bl/fw.c
 hantek_6022bl_objects = hw/hantek-6022bl/fw.rel
 
+hantek_pso2020_headers = $(scope_headers)
+hantek_pso2020_sources = hw/hantek-pso2020/fw.c
+hantek_pso2020_objects = hw/hantek-pso2020/fw.rel
+
 instrustar_isds205b_headers = $(scope_headers)
 instrustar_isds205b_sources = hw/instrustar-isds205b/fw.c
 instrustar_isds205b_objects = hw/instrustar-isds205b/fw.rel
@@ -247,6 +253,7 @@ hw_sources = \
 	hw/cypress-fx2/dscr.a51 \
 	hw/hantek-6022be/dscr.a51 \
 	hw/hantek-6022bl/dscr.a51 \
+	hw/hantek-pso2020/dscr.a51 \
 	hw/instrustar-isds205b/dscr.a51 \
 	hw/sainsmart-dds120/dscr.a51 \
 	hw/saleae-logic/dscr.a51 \
@@ -261,6 +268,7 @@ dist_noinst_DATA = \
 	$(fx2lafw_headers) $(fx2lafw_sources) \
 	$(hantek_6022be_headers) $(hantek_6022be_sources) \
 	$(hantek_6022bl_headers) $(hantek_6022bl_sources) \
+	$(hantek_pso2020_headers) $(hantek_pso2020_sources) \
 	$(instrustar_isds205b_headers) $(instrustar_isds205b_sources) \
 	$(sainsmart_dds120_sources) $(sainsmart_dds120_headers) \
 	$(yixingdianzi_mdso_sources) $(yixingdianzi_mdso_headers) \
@@ -329,6 +337,7 @@ hw/cwav-usbeezx/dscr.rel: hw/cwav-usbeezx/$(dirstamp) include/dscr.inc
 hw/cypress-fx2/dscr.rel: hw/cypress-fx2/$(dirstamp) include/dscr.inc
 hw/hantek-6022be/dscr.rel: hw/hantek-6022be/$(dirstamp) include/dscr_scope.inc
 hw/hantek-6022bl/dscr.rel: hw/hantek-6022bl/$(dirstamp) include/dscr_scope.inc
+hw/hantek-pso2020/dscr.rel: hw/hantek-pso2020/$(dirstamp) include/dscr_scope.inc
 hw/instrustar-isds205b/dscr.rel: hw/instrustar-isds205b/$(dirstamp) include/dscr_scope.inc
 hw/sainsmart-dds120/dscr.rel: hw/sainsmart-dds120/$(dirstamp) include/dscr_scope.inc
 hw/saleae-logic/dscr.rel: hw/saleae-logic/$(dirstamp) include/dscr.inc
@@ -373,6 +382,9 @@ hw/hantek-6022be/fx2lafw-hantek-6022be.ihx: hw/hantek-6022be/dscr.rel $(hantek_6
 hw/hantek-6022bl/fx2lafw-hantek-6022bl.ihx: hw/hantek-6022bl/dscr.rel $(hantek_6022bl_objects) $(fx2lib_libs)
 	$(AM_V_GEN)$(SDCC) -mmcs51 $(SDCC_LINK_FLAGS_SCOPE) -o $@ hw/hantek-6022bl/dscr.rel $(hantek_6022bl_objects) $(fx2lib_libs)
 
+hw/hantek-pso2020/fx2lafw-hantek-pso2020.ihx: hw/hantek-pso2020/dscr.rel $(hantek_pso2020_objects) $(fx2lib_libs)
+	$(AM_V_GEN)$(SDCC) -mmcs51 $(SDCC_LINK_FLAGS_SCOPE) -o $@ hw/hantek-pso2020/dscr.rel $(hantek_pso2020_objects) $(fx2lib_libs)
+
 hw/instrustar-isds205b/fx2lafw-instrustar-isds205b.ihx: hw/instrustar-isds205b/dscr.rel $(instrustar_isds205b_objects) $(fx2lib_libs)
 	$(AM_V_GEN)$(SDCC) -mmcs51 $(SDCC_LINK_FLAGS_SCOPE) -o $@ hw/instrustar-isds205b/dscr.rel $(instrustar_isds205b_objects) $(fx2lib_libs)
 
diff --git a/hw/hantek-pso2020/dscr.a51 b/hw/hantek-pso2020/dscr.a51
new file mode 100644
index 00000000..c1877c8a
--- /dev/null
+++ b/hw/hantek-pso2020/dscr.a51
@@ -0,0 +1,27 @@
+;;
+;; This file is part of the sigrok-firmware-fx2lafw project.
+;;
+;; Copyright (C) 2024 Steve Markgraf <steve@steve-m.de>
+;;
+;; This program is free software; you can redistribute it and/or modify
+;; it under the terms of the GNU General Public License as published by
+;; the Free Software Foundation; either version 2 of the License, or
+;; (at your option) any later version.
+;;
+;; This program is distributed in the hope that it will be useful,
+;; but WITHOUT ANY WARRANTY; without even the implied warranty of
+;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+;; GNU General Public License for more details.
+;;
+;; You should have received a copy of the GNU General Public License
+;; along with this program; if not, see <http://www.gnu.org/licenses/>.
+;;
+
+VID = 0x501d	; Manufacturer ID (0x1d50)
+PID = 0x8e60	; Product ID (0x608e)
+VER = 0x0600	; Product "version". 0x0006 == Hantek PSO2020.
+
+.include "dscr_scope.inc"
+string_descriptor_a 3,^"Hantek PSO2020"
+_dev_strings_end:
+	.dw	0x0000
diff --git a/hw/hantek-pso2020/fw.c b/hw/hantek-pso2020/fw.c
new file mode 100644
index 00000000..fb983634
--- /dev/null
+++ b/hw/hantek-pso2020/fw.c
@@ -0,0 +1,138 @@
+/*
+ * This file is part of the sigrok-firmware-fx2lafw project.
+ *
+ * Copyright (C) 2009 Ubixum, Inc.
+ * Copyright (C) 2015 Jochen Hoenicke
+ * Copyright (C) 2024 Steve Markgraf
+ *
+ * This library is free software; you can redistribute it and/or
+ * modify it under the terms of the GNU Lesser General Public
+ * License as published by the Free Software Foundation; either
+ * version 2.1 of the License, or (at your option) any later version.
+ *
+ * This library is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+ * Lesser General Public License for more details.
+ *
+ * You should have received a copy of the GNU Lesser General Public
+ * License along with this library; if not, see <http://www.gnu.org/licenses/>.
+ */
+
+#include <fx2macros.h>
+#include <fx2ints.h>
+#include <autovector.h>
+#include <delay.h>
+#include <setupdat.h>
+
+#define SET_ANALOG_MODE()
+
+#define SET_COUPLING(x) set_coupling_pso2020(x)
+
+#define SET_CALIBRATION_PULSE(x)
+
+#define TOGGLE_CALIBRATION_PIN()
+
+#define LED_CLEAR() do { PA6 = 1; IOE |= (1 << 2); IOE &= ~(1 << 1); } while (0)
+#define LED_GREEN() do { PA6 = 0; IOE |= (1 << 2); } while (0)
+#define LED_RED()   do { PA6 = 1; IOE &= ~(1 << 2); } while (0)
+#define LED_WHITE() do { IOE |= (1 << 1); } while (0)
+
+#define MUX_S0 (1 << 2)
+#define MUX_S1 (1 << 1)
+
+#define TIMER2_VAL 500
+
+/* CTLx pin index (IFCLK, ADC clock input). */
+#define CTL_BIT 0
+
+#define OUT0 ((1 << CTL_BIT) << 4) /* OEx = 1, CTLx = 0 */
+
+static const struct samplerate_info samplerates[] = {
+	{ 48, 0x80,   0, 3, 0, 0x00, 0xea },
+	{ 30, 0x80,   0, 3, 0, 0x00, 0xaa },
+	{ 24,    1,   0, 2, 1, OUT0, 0xca },
+	{ 16,    1,   1, 2, 0, OUT0, 0xca },
+	{ 12,    2,   1, 2, 0, OUT0, 0xca },
+	{  8,    3,   2, 2, 0, OUT0, 0xca },
+	{  4,    6,   5, 2, 0, OUT0, 0xca },
+	{  2,   12,  11, 2, 0, OUT0, 0xca },
+	{  1,   24,  23, 2, 0, OUT0, 0xca },
+	{ 50,   48,  47, 2, 0, OUT0, 0xca },
+	{ 20,  120, 119, 2, 0, OUT0, 0xca },
+	{ 10,  240, 239, 2, 0, OUT0, 0xca },
+};
+
+/**
+ * Control analog mux and relays to switch voltage ranges
+ */
+static BOOL set_voltage(BYTE channel, BYTE val)
+{
+	BYTE bits = 0;
+	BYTE mask = MUX_S0 | MUX_S1;
+
+	/* ADC channel B is only used for interleaved sampling
+	 * to get 96 MHz sampling rate, so it shares the
+	 * analog input circuitry with channel A
+	 * Instead of returning an error, silently ignore it */
+	if (channel > 0)
+		return TRUE;
+
+	switch (val) {
+	case 1:
+		/* 10 and 20V */
+		bits = MUX_S1;
+		break;
+	case 2:
+		/* 5V */
+		bits = MUX_S0;
+		break;
+	case 5:
+		/* 2V */
+		break;
+	case 10:
+		/* 1V */
+		bits = MUX_S0 | MUX_S1;
+		break;
+	case 11:
+		/* 500 mV */
+		bits = MUX_S1;
+		break;
+	case 12:
+		/* 200mV */
+		bits = MUX_S0;
+		break;
+	case 13:
+		/* 100mV */
+		break;
+	case 14:
+		/* 20 and 50mV in original software */
+		bits = MUX_S0 | MUX_S1;
+		break;
+	default:
+		return FALSE;
+	}
+
+	/* Switch relay for high voltage range */
+	if (val <= 10)
+		PA7 = 1;
+	else
+		PA7 = 0;
+
+	IOC = (IOC & ~mask) | (bits & mask);
+
+	return TRUE;
+}
+
+/**
+ * Switch between AC and DC coupling
+ */
+static void set_coupling_pso2020(BYTE coupling_cfg)
+{
+	if (coupling_cfg & 0x01)
+		PC4 = 0;
+	else
+		PC4 = 1;
+}
+
+#include <scope.inc>
